package com.lingtu.lbs.common;


import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName DefaultView
 * @Description 项目默认首页
 * @Author MaShuai
 * @Date 2018/11/7 17:20
 * @Version 1.0
 **/
@Configuration
/* @EnableWebMvc 添加该注解，则是完全控制MVC，会导致js、css的引入失败问题*/
public class DefaultView implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("cell");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

}
