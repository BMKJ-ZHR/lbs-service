package com.lingtu.lbs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lingtu.lbs.mapper")
public class LbsServiceApplication{

    public static void main(String[] args) {
        SpringApplication.run(LbsServiceApplication.class, args);
    }

   /* @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(LbsServiceApplication.class);
    }*/

}
