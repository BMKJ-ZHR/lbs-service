package com.lingtu.lbs.mapper;

import com.lingtu.lbs.dto.params.LbsParam;
import com.lingtu.lbs.pojo.Lbs;
/**
 * @InterfaceName: LbsMapper
 * @Description:
 * @Author MaShuai
 * @Date 2018/11/5 21:55
 * @Version V1.0
 */
public interface LbsMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Lbs record);

    int insertSelective(Lbs record);

    Lbs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Lbs record);

    int updateByPrimaryKey(Lbs record);

    Lbs selectLbsByParam(LbsParam lbsParam);
}
