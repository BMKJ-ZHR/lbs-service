package com.lingtu.lbs.utils.http;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
 * @ClassName HttpProxy
 * @Description http代理
 * @Author MaShuai
 * @Date 2018/11/9 17:24
 * @Version 1.0
 */
@Component
@ConfigurationProperties(prefix="http-proxy") //接收application.yml中的http-proxy下面的属性
public class HttpProxy {
    private String scheme;
    private String host;
    private int port;
    private String username;
    private String password;
    private int timeOut;

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    @Override
    public String toString() {
        return "HttpProxy{" +
                "scheme='" + scheme + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", timeOut=" + timeOut +
                '}';
    }
}

