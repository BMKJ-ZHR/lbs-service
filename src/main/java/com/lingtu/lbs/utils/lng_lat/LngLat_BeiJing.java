package com.lingtu.lbs.utils.lng_lat;

import com.alibaba.fastjson.JSONObject;
import com.lingtu.lbs.utils.http.HttpClient;

import java.util.Arrays;

/**
 * @ClassName LngLat_BeiJing
 * @Description 北京市经纬度
 * @Author MaShuai
 * @Date 2018/11/16 9:02
 * @Version 1.0
 **/
public class LngLat_BeiJing {

    /**
     *  北京市最小与最大经度
     */
    private static Double MIN_LAT;
    private static Double MAX_LAT;

    /**
     *  北京市最小与最大纬度
     */
    private static Double MIN_LNG;
    private static Double MAX_LNG;

    static {
        MIN_LAT = 115.00;
        MAX_LAT = 117.50;
        MIN_LNG = 39.00;
        MAX_LNG = 41.00;
    }

    public void getLBS(){
        while (MIN_LAT <= MAX_LAT){
            String s = HttpClient.doGet("http://api.cellocation.com:81/recell/?incoord=gcj02&coord=gcj02&lat=39.98373&lon=116.322428");
            MIN_LAT += 0.01;
        }
    }

    public static void main(String args[]){
        String s = HttpClient.doGet("http://api.cellocation.com:81/recell/?incoord=gcj02&coord=gcj02&lat=39&lon=115&n=10");
        s = s.substring(1,s.length()-1);
        String str[] = s.split("},\\{");
        for(int n=0;n<str.length;n++){
            System.out.println(str[n]);
        }
        //JSONObject jsonObject = JSONObject.parseObject(s);
        //System.out.println(s);

    }

}
