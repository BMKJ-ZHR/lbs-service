package com.lingtu.lbs.utils.result;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: Result
 * @Description: 统一API响应结果封装
 * @Author MaShuai
 * @Date 2018/11/5 22:18
 * @Version V1.0
 */
public class Result {

    private int status;
    private String msg;
    private Object data;

    public Result() {
    }

    public Result(ResultCodeEnum resultCodeEnum, String msg, Object data) {
        this.status = resultCodeEnum.status();
        this.msg = msg;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public Result setStatus(ResultCodeEnum resultCodeEnum) {
        this.status = resultCodeEnum.status();
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static Result error(ResultCodeEnum resultCodeEnum, String msg){
        return new Result(resultCodeEnum, msg, null);
    }

    public static Result ok(ResultCodeEnum resultCodeEnum, String msg){
        return new Result(resultCodeEnum, msg, null);
    }

    public static Result build(ResultCodeEnum resultCodeEnum, String msg, Object data){
        return new Result(resultCodeEnum, msg, data);
    }



    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}

