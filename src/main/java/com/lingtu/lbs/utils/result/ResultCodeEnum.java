package com.lingtu.lbs.utils.result;

/**
 * @Author MaShuai
 * @Description 响应码枚举，参考HTTP状态码的语义
 * @Date 2018/11/5 22:19
 **/
public enum ResultCodeEnum {

    // 成功
    SUCCESS(200),

    // 失败
    FAIL(400),

    // 未认证（签名错误）
    UNAUTHORIZED(401),

    // 接口不存在
    NOT_FOUND(404),

    // 服务器内部错误
    INTERNAL_SERVER_ERROR(500),

    // 参数错误
    PARAMETER_ERROR(402);

    private int status;

    ResultCodeEnum(int status) {
        this.status = status;
    }

    public int status() {
        return status;
    }
}
