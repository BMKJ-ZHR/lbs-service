package com.lingtu.lbs.pojo;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: Lbs
 * @Description: 基站实体类
 * @Author MaShuai
 * @Date 2018/11/5 21:49
 * @Version V1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Lbs implements Serializable {
    private Integer id;

    private Integer mcc;

    private Integer mnc;

    private Long lac;

    private Long cell;

    private Double lng;

    private Double lat;

    private Double oLng;

    private Double oLat;

    private Long coverageRadius;

    private String address;

    private Date upDate;

    private String region;

    private String city;

    private String country;

}
