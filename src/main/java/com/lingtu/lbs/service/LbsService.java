package com.lingtu.lbs.service;

import com.lingtu.lbs.utils.result.Result;
import com.lingtu.lbs.dto.params.LbsParam;
import com.lingtu.lbs.pojo.Lbs;

/**
 * @InterfaceName: LbsService
 * @Description: lbs 业务接口
 * @Author MaShuai
 * @Date 2018/11/5 22:17
 * @Version V1.0
 */
public interface LbsService {

    /**
     * @Description 查询单个基站信息
     * @Param [lbsParam]
     * @return com.lingtu.lbs.utils.result.Result<com.lingtu.lbs.pojo.Lbs>
     */
    Result findLbsByParam(LbsParam lbsParam);

}
