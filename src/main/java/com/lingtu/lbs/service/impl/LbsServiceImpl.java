package com.lingtu.lbs.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lingtu.lbs.mapper.LbsMapper;
import com.lingtu.lbs.utils.http.HttpClient;
import com.lingtu.lbs.utils.http.HttpProxy;
import com.lingtu.lbs.utils.result.Result;
import com.lingtu.lbs.dto.params.LbsParam;
import com.lingtu.lbs.pojo.Lbs;
import com.lingtu.lbs.service.LbsService;
import com.lingtu.lbs.utils.result.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @ClassName: LbsServiceImpl
 * @Description: lbs业务实现类
 * @Author MaShuai
 * @Date 2018/11/5 22:28
 * @Version V1.0
 */
@Service("LbsService")
public class LbsServiceImpl implements LbsService {

    /**
     LBS 基站查询结果状态码：errcode
     0: 成功
     10000: 参数错误
     10001: 无查询结果*/
    private static int API_SECCESS = 0;
    private static int API_BADPARAM = 10000;
    private static int API_NORESULT = 10001;

    @Autowired
    private LbsMapper lbsMapper;

    @Autowired
    private HttpProxy httpProxy;

    /**
     * @Description 根据四个必须的参数查询单个基站信息
     * @Param [lbsParam](mcc, mnc, lac, cell)
     * @return com.lingtu.lbs.utils.result.Result<com.lingtu.lbs.pojo.Lbs>
     */
    @Override
    public Result findLbsByParam(LbsParam lbsParam) {
        if(lbsParam == null){
            return Result.error(ResultCodeEnum.PARAMETER_ERROR, "参数错误!");
        }

        Lbs lbs = lbsMapper.selectLbsByParam(lbsParam);
        // 调用LBS接口查询新数据
        Lbs newLbs = new Lbs();
        Result result = getOtherLbs(lbsParam);
        if(result.getStatus() == 200) {
            JSONObject jsonObject = JSONObject.parseObject(result.getData().toString());
            newLbs.setMcc(lbsParam.getMcc());
            newLbs.setMnc(lbsParam.getMnc());
            newLbs.setLac(lbsParam.getLac());
            newLbs.setCell(lbsParam.getCell());
            newLbs.setLng(jsonObject.getDouble("lon"));
            newLbs.setLat(jsonObject.getDouble("lat"));
            newLbs.setOLng(null);
            newLbs.setOLat(null);
            newLbs.setCoverageRadius(jsonObject.getLong("radius"));
            newLbs.setAddress(jsonObject.getString("address"));
            newLbs.setUpDate(new Date());
        }

        // 如果数据库没有数据，则将新数据插入到数据库，最后返回新数据
        if(lbs == null && result.getStatus() == 200) {
            lbsMapper.insertSelective(newLbs);
            return Result.build(ResultCodeEnum.SUCCESS, "数据获取成功", newLbs);
        } else
        // 如果数据库查询到数据，则对比坐标，如果坐标相同，则返回原数据,不相同则更新到数据库
        if(lbs != null && result.getStatus() == 200) {
            if(lbs.getLng().equals(newLbs.getLng()) && lbs.getLat().equals(newLbs.getLat())){
                return Result.build(ResultCodeEnum.SUCCESS, "数据获取成功", lbs);
            } else {
                newLbs.setId(lbs.getId());
                lbsMapper.updateByPrimaryKeySelective(newLbs);
                return Result.build(ResultCodeEnum.SUCCESS, "数据获取成功", newLbs);
            }
        } else
        // 如果数据库查询到数据，而接口未能查询到数据，则返回原数据
        if(lbs != null && result.getStatus() != 200){
            return Result.build(ResultCodeEnum.SUCCESS, "数据获取成功", lbs);
        } else
        if( result.getStatus() == 402){
            // 402
            return Result.error(ResultCodeEnum.PARAMETER_ERROR, "亲，参数错误!");
        }
        // 如果数据库与接口都未能查询到数据
        return Result.error(ResultCodeEnum.FAIL, "亲，暂无此基站的数据，我们会继续更新！");

    }

    /**
     * @Description 调用LBS接口查询数据
     * @Param [lbsParam]
     * @return com.lingtu.lbs.utils.result.Result
     */
    public Result getOtherLbs(LbsParam lbsParam){
        if(lbsParam == null){
            return Result.error(ResultCodeEnum.PARAMETER_ERROR, "参数错误!");
        }

        Integer mcc = lbsParam.getMcc();
        Integer mnc = lbsParam.getMnc();
        Long lac = lbsParam.getLac();
        Long ci = lbsParam.getCell();
        /** api接口："http://api.cellocation.com:81/cell/?coord=gcj02&output=json&mcc=460&mnc=0&lac=4462&ci=18280205 */
        String s = HttpClient.doGet("http://api.cellocation.com:81/cell/?coord=gcj02&output=json&mcc="+mcc+"&mnc="+mnc+"&lac="+lac+"&ci="+ci, httpProxy);

        JSONObject jsonObject = JSONObject.parseObject(s);
        int status = jsonObject.getInteger("errcode");
        if(status == API_NORESULT){
            return Result.error(ResultCodeEnum.FAIL, "亲，无查询结果!");
        }
        if(status == API_BADPARAM){
            return Result.error(ResultCodeEnum.PARAMETER_ERROR, "亲，参数错误!");
        }

        return Result.build(ResultCodeEnum.SUCCESS, "远程查询成功！", jsonObject);

    }

}
