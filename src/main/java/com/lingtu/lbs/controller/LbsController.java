package com.lingtu.lbs.controller;

import com.alibaba.fastjson.JSONObject;
import com.lingtu.lbs.dto.params.LbsParam;
import com.lingtu.lbs.service.LbsService;
import com.lingtu.lbs.utils.http.HttpClient;
import com.lingtu.lbs.utils.http.HttpProxy;
import com.lingtu.lbs.utils.logs.LogUtil;
import com.lingtu.lbs.utils.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName: LbsController
 * @Description: lbs控制层
 * @Author MaShuai
 * @Date 2018/11/5 22:49
 * @Version V1.0
 */
@Slf4j
@Controller
@RequestMapping(value = "/lbs")
public class LbsController {
    @Autowired
    LbsService lbsService;
    @Autowired
    private HttpProxy httpProxy;

    @ResponseBody
    @RequestMapping("/find")
    public Result lbs(LbsParam lbsParam){

        Logger log = LogUtil.getExceptionLogger();
        Logger log1 = LogUtil.getBussinessLogger();
        Logger log2 = LogUtil.getDBLogger();

        log.error("getExceptionLogger===日志测试");
        log1.info("getBussinessLogger===日志测试");
        log2.debug("getDBLogger===日志测试");

        return lbsService.findLbsByParam(lbsParam);
    }


}
