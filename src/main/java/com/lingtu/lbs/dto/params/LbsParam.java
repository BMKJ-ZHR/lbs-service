package com.lingtu.lbs.dto.params;

import java.io.Serializable;

/**
 * @ClassName: LbsParam
 * @Description: 查询LBS所需参数实体类
 * @Author MaShuai
 * @Date 2018/11/5 21:56
 * @Version V1.0
 */

public class LbsParam implements Serializable {

    private Integer mcc;

    private Integer mnc;

    private Long lac;

    private Long cell;

    public Integer getMcc() {
        return mcc;
    }

    public void setMcc(Integer mcc) {
        this.mcc = mcc;
    }

    public Integer getMnc() {
        return mnc;
    }

    public void setMnc(Integer mnc) {
        this.mnc = mnc;
    }

    public Long getLac() {
        return lac;
    }

    public void setLac(Long lac) {
        this.lac = lac;
    }

    public Long getCell() {
        return cell;
    }

    public void setCell(Long cell) {
        this.cell = cell;
    }

    @Override
    public String toString() {
        return "LbsParam{" +
                "mcc=" + mcc +
                ", mnc=" + mnc +
                ", lac=" + lac +
                ", cell=" + cell +
                '}';
    }
}
