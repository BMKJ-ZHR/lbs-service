/**
 * @Author MaShuai
 * @Description cell脚本
 * @Date 2018/11/8 11:08
 **/

$(function(){
    mapInit();
});

/**
 * @Description 初始化地图
 */
var mapObj,marker;
function mapInit(){
    mapObj = new AMap.Map("iCenter",{
        center:[116.292615,40.04187],
        zoom:14
    });
}

/**
 * @Description 添加地图点标记
 */
function addMarker(){
    var mcc = document.getElementById("mcc").value;
    var mnc = document.getElementById("mnc").value;
    var lac = document.getElementById("lac").value;
    var cell = document.getElementById("cell").value;

    var lbs = findLbs(parseInt(mcc),parseInt(mnc), parseInt(lac), parseInt(cell));
    if(lbs == null){
        return false;
    } else {
        var lon = lbs.lng;
        var lat = lbs.lat;
        var addr = lbs.address;
        var radius = lbs.coverageRadius;

        var circle = new AMap.Circle({
            center: [lon,lat],  // 圆心位置
            radius: radius, // 圆半径
            strokeColor: "#FF33FF", // 线条颜色,此处设置透明
            strokeOpacity: 0, // 轮廓线透明度，取值范围[0,1]，0表示完全透明，1表示不透明。默认为0.9
            strokeWeight: 6, // 轮廓线宽度
            fillOpacity: 0.4, // 圆形填充透明度，取值范围[0,1]，0表示完全透明，1表示不透明。默认为0.9
            strokeStyle: 'dashed', // 轮廓线样式，实线:solid，虚线:dashed
            strokeDasharray: [10, 10],
            // 线样式还支持 'dashed'
            fillColor: '#1791fc', // 圆形填充颜色
            zIndex: 50, // 层叠顺序, 默认10
        });

        mapObj.add(circle);

        marker = new AMap.Marker({
            position:new AMap.LngLat(lon,lat)
        });

        mapObj.add(marker);
        inforWindow = new AMap.InfoWindow({
            autoMove:true,
            offset:new AMap.Pixel(0,-30)
        });
        var info = [];
        info.push(mcc + "," + mnc + "," + lac + "," + cell);
        info.push(lat + "," + lon + " (GCJ02)");
        info.push(addr);
        inforWindow.setContent(info.join("<br/>"));
        inforWindow.open(mapObj, new AMap.LngLat(lon,lat));
        mapObj.setZoomAndCenter(14, new AMap.LngLat(lon,lat));
    }


}

/**
 * @Description 根据四个参数查询基站信息
 */
function findLbs(mcc,mnc,lac,cell){
    if(mcc == null || mnc == null || lac == null || cell == null ){
        return false;
    }
    var lbs= {};
    $.ajax({
        async:false,
        url:"/lbs/find",
        type:"post",
        dataType:"json",
        data:{"mcc":mcc, "mnc":mnc, "lac":lac, "cell":cell},
        success:function(result){
            console.log(result);
            if (result.status == 200) {
                if (result.data != null) {
                    lbs = result.data;
                } else {
                    alert("服务错误！");
                }
            } else {
                alert(result.msg);
                return false;
            }
        }
    });
    return lbs;
}
